import Component from '@ember/component';

function heuristic(current, target){
    return Math.abs(target[0] - current[0]) + Math.abs(target[1] - current[1]);
}
function distance(current, target, squares){
    return squares[current[0]][current[1]].distance + heuristic(current, target);
}

function fillSquare(square, colour) {
    var graphics = square.graphics;
    graphics.beginFill(colour);
    graphics.beginStroke('#ffffff');
    graphics.setStrokeStyle(2);
    graphics.drawRect(0, 0,30, 30);
}

export default Component.extend({
    ready: true,
    running: false,

    didInsertElement: function() {
        var stage =  new createjs.Stage(this.$('#stage')[0]);

        var squares = [];
        for(var idx1 = 0; idx1 < 10; idx1++){
            var line = [];
            for(var idx2 = 0; idx2 < 10; idx2++) {
                var square = new createjs.Shape();
                fillSquare(square, '#000000');
                square.x = idx1 * 30;
                square.y = idx2 * 30;
                square.status = 'free';
                stage.addChild(square);
                line.push(square);
            }
            squares.push(line);
        }
        this.set('squares', squares);
        stage.update();
        this.set('stage', stage);
    },

    click: function(ev) {
        if(ev.target.nodeName === 'CANVAS' && ev.offsetX > 0 && ev.offsetY > 0 && ev.offsetX < 300 && ev.offsetY < 300) {
            var x = Math.floor(ev.offsetX / 30);
            var y = Math.floor(ev.offsetY / 30);
            var squares = this.get('squares');
            var square = squares[x][y];
            if(square.status === 'free') {
                fillSquare(square, '#ffffff');
                square.status = 'blocked';
            }else if(square.status === 'blocked') {
                fillSquare(square, '#0000ff');
                square.status = 'start';
            }else if(square.status === 'start') {
                fillSquare(square, '#00ff00');
                square.status = 'end';
            } else{
            fillSquare(square, '#000000');
            square.status = 'free';
            }
            this.get('stage').update();
        }
    },

    actions: {
        clear: function() {
            var squares =this.get('squares');
            for(var idx1 = 0; idx1 < 10; idx1++) {
                for(var idx2 = 0; idx2 < 10; idx2++) {
                    squares[idx1][idx2].status = 'free';
                    fillSquare(squares[idx1][idx2], '#000000');
                }
            }
            this.set('ready', true);
            this.get('stage').update();
        },

        start: function() {
            var squares = this.get('squares');
            var start = undefined;
            var end = undefined;
            for(var idx1 = 0; idx1 < 10; idx1 ++){
                for(var idx2 = 0; idx2 < 10; idx2++){
                    squares[idx1][idx2].distance = undefined;
                    if(!start && squares[idx1][idx2].status === 'start') {
                        start = [idx1, idx2];
                    }
                    if(!end && squares[idx1][idx2].status === 'end'){
                        end = [idx1, idx2];
                    }
                }
            }
            if(start !== undefined && end !== undefined) {
                squares[start[0]][start[1]].distance = 0;
                this.set('start', start);
                this.set('end', end);
                this.set('unvisited', [[start[0], start[1]]]);
                this.set('running', true);
                this.set('ready', false);
            }
        },

        step: function() {
            var squares = this.get('squares');
            var unvisited = this.get('unvisited');
            var end = this.get('end');
            var current = unvisited.shift();
            if(current[0] == end[0] && current[1] == end[1]){
                fillSquare(squares[current[0]][current[1]], '#00aa00');
                this.set('running', false);
            }else{
                fillSquare(squares[current[0]][current[1]], '#aa0000');
                for(var idx1 = 0; idx1 < 10; idx1++){
                    for(var idx2 = 0; idx2 < 10; idx2++){
                        if(squares[idx1][idx2].distance === undefined && squares[idx1][idx2].status !== 'blocked'){
                           if((idx1 !== current[0] && idx2 === current[1]) || (idx1 === current[0] && idx2 !== current[1])){
                               if(Math.abs(current[0] - idx1) <= 1 && Math.abs(current[1] - idx2) <= 1) {
                                   squares[idx1][idx2].distance = squares[current[0]][current[1]].distance + 1;
                                   unvisited.splice(0, 0, [idx1, idx2]);
                                   fillSquare(squares[idx1][idx2], '#666666');
                                   
                                   fillSquare(squares[end[0]][end[1]], '#00ff00');
                                   unvisited.sort(function(a, b){
                                       return distance(a, end, squares) - distance(b, end, squares);
                                   });
                               }
                           } 
                        }
                    }
                }
            }
            this.get('stage').update();
        },
    }

});
